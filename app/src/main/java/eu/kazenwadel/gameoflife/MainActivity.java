package eu.kazenwadel.gameoflife;


import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageOfGame;
    Bitmap lastBitmap;
    Bitmap newBitmap;

    final public void updateUI(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageOfGame.setImageBitmap(newBitmap);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageOfGame = findViewById(R.id.gamePicture);

        lastBitmap = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888);
        newBitmap = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888);
        count();
    }

    public void count () {
        new Thread(new Runnable() {
            public void run() {

                int sizex=100;
                int sizey=100;
                boolean pixels[][] = new boolean[sizex][sizey];
                boolean newpixels[][] = new boolean[sizex][sizey];

                pixels[50][50]=true;
                pixels[51][50]=true;
                pixels[50][51]=true;
                pixels[49][51]=true;
                pixels[50][52]=true;

                while (true==true){
                    for (int i = 0; i<sizex; i++){
                        for (int j=0;j<sizey;j++){
                            int neighbors=0;
                            newpixels[i][j]=false;
                            newBitmap.setPixel(i,j,Color.WHITE);
                            for( int x = i -1; x< i+2; x++){
                                for( int y =j -1; y<j+2;y++){
                                    if(x != i || y != j){
                                        try {
                                            if (pixels[x][y] == true) {
                                                neighbors = neighbors + 1;
                                                Log.d("Neighboir", "found");
                                            }
                                        }
                                        catch(Exception e){
                                        }
                                    }
                                }
                            }

                            if (pixels[i][j]==false){
                                if (neighbors==3){
                                    newpixels[i][j]=true;
                                }
                            }
                            else{
                                if (neighbors==2||neighbors==3){
                                    newpixels[i][j]=true;
                                }
                                else{
                                    newpixels[i][j]=false;
                                }

                            }
                            if (newpixels[i][j]==true){
                                newBitmap.setPixel(i,j,Color.RED);
                            }
                            else{
                                newBitmap.setPixel(i,j,Color.WHITE);
                            }
                        }
                    }
                    for (int i = 0; i<sizex; i++) {
                        for (int j = 0; j < sizey; j++) {
                            pixels[i][j]=newpixels[i][j];
                        }
                    }
                    lastBitmap=newBitmap;
                    updateUI();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}